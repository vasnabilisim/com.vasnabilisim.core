package com.vasnabilisim.core;

import com.vasnabilisim.util.MessageType;

/**
 * Logger class for log4j.
 * 
 * @author Menderes Fatih GÜVEN
 */
public class Logger {


	private static org.apache.logging.log4j.Logger logger;
	
	/**
	 * Initializes logger to log to given path.
	 * @param logFolderPath
	 * @throws BaseException
	 */
	public static void init(String name, String logFolderPath) throws BaseException {
		logger = org.apache.logging.log4j.LogManager.getLogger("default");
	}
	
	/**
	 * Returns the logger instance.
	 * 
	 * @return
	 */
	public static org.apache.logging.log4j.Logger getLogger() {
		return logger;
	}

	/**
	 * Sets the logger instance.
	 * 
	 * @param logger
	 */
	public static void setLogger(org.apache.logging.log4j.Logger logger) {
		Logger.logger = logger;
	}

	/**
	 * Logs given exception according to its type.
	 * 
	 * @param e
	 */
	public static void log(BaseException e) {
		if (e.getType() == MessageType.Error)
			error(e.getMessage(), e);
		else if (e.getType() == MessageType.Warning)
			warning(e.getMessage());
		else if (e.getType() == MessageType.Info)
			info(e.getMessage());
		else if (e.getType() == MessageType.Fatal)
			fatal(e.getMessage(), e);
		else
			error(e.getMessage(), e);
	}

	/**
	 * Logs given throwable.
	 * 
	 * @param throwable
	 */
	public static void log(Throwable throwable) {
		if (throwable instanceof BaseException)
			log((BaseException)throwable);
		else
			error(throwable.getMessage(), throwable);
	}

	/**
	 * Logs a trace.
	 * 
	 * @param message
	 */
	public static void trace(String message) {
		logger.trace(message);
	}

	/**
	 * Logs an info.
	 * 
	 * @param message
	 */
	public static void info(String message) {
		logger.info(message);
	}

	/**
	 * Logs a warning.
	 * 
	 * @param message
	 */
	public static void warning(String message) {
		logger.warn(message);
	}

	/**
	 * Logs a warning.
	 * 
	 * @param message
	 * @param thrown
	 */
	public static void warning(String message, Throwable thrown) {
		logger.warn(message, thrown);
	}

	/**
	 * Logs an error.
	 * 
	 * @param message
	 */
	public static void error(String message) {
		logger.error(message);
	}

	/**
	 * Logs an error.
	 * 
	 * @param thrown
	 */
	public static void error(Throwable thrown) {
		logger.error(thrown != null ? thrown.getMessage() : null, thrown);
	}

	/**
	 * Logs an error.
	 * 
	 * @param message
	 * @param thrown
	 */
	public static void error(String message, Throwable thrown) {
		logger.error(message, thrown);
	}

	/**
	 * Logs an error.
	 * 
	 * @param message
	 * @param throwables
	 */
	public static void error(String message, Throwable... throwables) {
		for(Throwable thrown : throwables)
			logger.error(message, thrown);
	}

	/**
	 * Logs a fatal error.
	 * 
	 * @param message
	 */
	public static void fatal(String message) {
		logger.fatal(message);
	}

	/**
	 * Logs a fatal error.
	 * 
	 * @param thrown
	 */
	public static void fatal(Throwable thrown) {
		logger.fatal(thrown != null ? thrown.getMessage() : null, thrown);
	}

	/**
	 * Logs a fatal error.
	 * 
	 * @param message
	 * @param thrown
	 */
	public static void fatal(String message, Throwable thrown) {
		logger.fatal(message, thrown);
	}
}
