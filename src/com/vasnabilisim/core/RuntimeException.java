package com.vasnabilisim.core;

/**
 * Runtime exception class of the solution.
 * 
 * @author Menderes Fatih GUVEN
 */
public class RuntimeException extends java.lang.RuntimeException {
	private static final long serialVersionUID = 4516027975001995186L;

	/**
	 * Default constructor.
	 */
	public RuntimeException() {
		super();
	}
	
	/**
	 * Message constructor.
	 * @param message
	 */
	public RuntimeException(String message) {
		super(message);
	}
	
	/**
	 * Cause constructor.
	 * @param e
	 */
	public RuntimeException(Throwable e) {
		super(e);
	}
	
	/**
	 * Compound constructor.
	 * @param message
	 * @param e
	 */
	public RuntimeException(String message, Throwable e) {
		super(message, e);
	}
}
