package com.vasnabilisim.core;

import com.vasnabilisim.util.MessageType;

/**
 * Exception class of the solution.
 * 
 * @author Menderes Fatih GUVEN
 */
public class BaseException extends java.lang.Exception {
	private static final long serialVersionUID = -5365220892360179093L;

	private MessageType type;
	
	/**
	 * Type constructor.
	 * @param type
	 */
	public BaseException(MessageType type) {
		super();
		this.type = type;
	}
	
	/**
	 * Message constructor.
	 * @param type
	 * @param message
	 */
	public BaseException(MessageType type, String message) {
		super(message);
		this.type = type;
	}
	
	/**
	 * Cause constructor.
	 * @param type
	 * @param e
	 */
	public BaseException(MessageType type, Throwable e) {
		super(e);
		this.type = type;
	}
	
	/**
	 * Compound constructor.
	 * @param type
	 * @param message
	 * @param e
	 */
	public BaseException(MessageType type, String message, Throwable e) {
		super(message, e);
		this.type = type;
	}

	/**
	 * Returns the type.
	 * @return
	 */
	public MessageType getType() {
		return type;
	}
}
