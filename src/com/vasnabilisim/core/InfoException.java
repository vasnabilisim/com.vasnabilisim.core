package com.vasnabilisim.core;

import com.vasnabilisim.util.MessageType;

/**
 * InfoException
 * @author Menderes Fatih GUVEN
 */
public class InfoException extends BaseException {
	private static final long serialVersionUID = 357819602269681115L;

	public InfoException() {
		super(MessageType.Info);
	}

	/**
	 * @param message
	 */
	public InfoException(String message) {
		super(MessageType.Info, message);
	}

	/**
	 * @param cause
	 */
	public InfoException(Throwable cause) {
		super(MessageType.Info, cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public InfoException(String message, Throwable cause) {
		super(MessageType.Info, message, cause);
	}

}
