package com.vasnabilisim.core;

import com.vasnabilisim.util.MessageType;

/**
 * WarningException
 * @author Menderes Fatih GUVEN
 */
public class WarningException extends BaseException {
	private static final long serialVersionUID = 7972026181333696688L;

	public WarningException() {
		super(MessageType.Warning);
	}

	/**
	 * @param message
	 */
	public WarningException(String message) {
		super(MessageType.Warning, message);
	}

	/**
	 * @param cause
	 */
	public WarningException(Throwable cause) {
		super(MessageType.Warning, cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public WarningException(String message, Throwable cause) {
		super(MessageType.Warning, message, cause);
	}

}
