package com.vasnabilisim.core;

import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;


/**
 * Application context class. 
 * Uninstantiable. 
 * Use static methods for functionality.
 * 
 * @author Menderes Fatih GUVEN
 */
public class ApplicationContext {
    private static final TreeMap<String, Object> map = new TreeMap<String, Object>();

    /**
     * Type can not be instantiated.
     */
    private ApplicationContext() {}

    /**
     * Puts an object to session identified by given key.
     * @param key java.lang.String
     * @param model java.lang.Object
     */
    public static void put(String key, Object value) {
        map.put(key, value);
    }

    /**
     * Copies all of given source contentPane to itself.
     * @param source java.util.Map
     */
    public static void putAll(Map<String, Object> source) {
        ApplicationContext.map.putAll(source);
    }
    
    /**
     * Copies all of given source contentPane to itself.
     * @param props
     */
    public static void putAll(Properties props) {
    	for(Object key : props.keySet()) {
    		if(key instanceof String)
    			ApplicationContext.put((String)key, props.get(key));
    	}
    }
    
    /**
     * Does any object exists identified by given key.
     * @param key
     * @return
     */
    public static boolean contains(String key) {
    	return ApplicationContext.map.containsKey(key);
    }
    
    /**
     * Returns the object identified by given key.
     * @return java.lang.Object
     * @param key java.lang.String
     */
    public static Object get(String key) {
        return get(key, false);
    }

    /**
     * Returns the object identified by given key. 
     * If not found an IllegalArgumentException thrown.
     * @param key
     * @param must
     * @return
     * @throws IllegalArgumentException
     */
    public static Object get(String key, boolean must) throws IllegalArgumentException {
		Object value = map.get(key);
		if(value != null)
			return value;
		if(must) {
			String message = String.format("Unable to find object identified by %s.", key);
			throw new IllegalArgumentException(message);
		}
		return null;
	}

    /**
     * Returns the object identified by given key.
     * @param <T>
     * @param key
     * @param type
     * @return
     */
	public static <T> T get(String key, Class<T> type) {
    	return get(key, type, false);
    }    
    
    /**
     * Returns the object identified by given key. 
     * If not found an IllegalArgumentException thrown.
     * @param <T>
     * @param key
     * @param type
     * @param must
     * @return
     */
	public static <T> T get(String key, Class<T> type, boolean must) {
    	return type.cast(get(key, must));
    }
    
    /**
     * Returns the string identified by given key.
     * @param key
     * @return
     */
    public static String getString(String key) {
    	return getString(key, false);
    }

    /**
     * Returns the string identified by given key. 
     * If not found an IllegalArgumentException thrown.
     * @param key
     * @param must
     * @return
     * @throws IllegalArgumentException
     */
    public static String getString(String key, boolean must) {
    	return get(key, String.class, must);
    }
    
    /**
     * Returns the string identified by given key. 
     * If not returns given default value.
     * @param key
     * @param defaultValue
     * @return
     */
    public static String getString(String key, String defaultValue) {
    	String value = getString(key, false);
    	return value == null ? defaultValue : value;
    }
    
    /**
     * Returns the integer identified by given key.
     * @param key
     * @return
     */
    public static Integer getInteger(String key) {
    	return getInteger(key, false);
    }

    /**
     * Returns the integer identified by given key. 
     * If not found an IllegalArgumentException thrown.
     * @param key
     * @param must
     * @return
     * @throws IllegalArgumentException
     */
    public static Integer getInteger(String key, boolean must) {
    	return get(key, Integer.class, must);
    }
    
    /**
     * Returns the long identified by given key.
     * @param key
     * @return
     */
    public static Long getLong(String key) {
    	return getLong(key, false);
    }

    /**
     * Returns the long identified by given key. 
     * If not found an IllegalArgumentException thrown.
     * @param key
     * @param must
     * @return
     * @throws IllegalArgumentException
     */
    public static Long getLong(String key, boolean must) {
    	return get(key, Long.class, must);
    }

    /**
     * Returns the long identified by given key. 
     * If not returns given default value.
     * @param key
     * @param defaultValue
     * @return
     */
    public static Long getLong(String key, Long defaultValue) {
    	Long value = getLong(key, false);
		return value == null ? defaultValue : value;
    }
    
    
    /**
     * Returns the float identified by given key.
     * @param key
     * @return
     */
    public static Float getFloat(String key) {
    	return getFloat(key, false);
    }

    /**
     * Returns the float identified by given key. 
     * If not found an IllegalArgumentException thrown.
     * @param key
     * @param must
     * @return
     * @throws IllegalArgumentException
     */
    public static Float getFloat(String key, boolean must) {
    	return get(key, Float.class, must);
    }
    
    /**
     * Returns the double identified by given key.
     * @param key
     * @return
     */
    public static Double getDouble(String key) {
    	return getDouble(key, false);
    }

    /**
     * Returns the double identified by given key. 
     * If not found an IllegalArgumentException thrown.
     * @param key
     * @param must
     * @return
     * @throws IllegalArgumentException
     */
    public static Double getDouble(String key, boolean must) {
    	return get(key, Double.class, must);
    }
    
    /**
     * Returns the byte identified by given key.
     * @param key
     * @return
     */
    public static Byte getByte(String key) {
    	return getByte(key, false);
    }

    /**
     * Returns the byte identified by given key. 
     * If not found an IllegalArgumentException thrown.
     * @param key
     * @param must
     * @return
     * @throws IllegalArgumentException
     */
    public static Byte getByte(String key, boolean must) {
    	return get(key, Byte.class, must);
    }
    
    /**
     * Returns the character identified by given key.
     * @param key
     * @return
     */
    public static Character getCharacter(String key) {
    	return getCharacter(key, false);
    }

    /**
     * Returns the character identified by given key. 
     * If not found an IllegalArgumentException thrown.
     * @param key
     * @param must
     * @return
     * @throws IllegalArgumentException
     */
    public static Character getCharacter(String key, boolean must) {
    	return get(key, Character.class, must);
    }
    
    /**
     * Returns the boolean identified by given key.
     * @param key
     * @return
     */
    public static Boolean getBoolean(String key) {
    	return getBoolean(key, false);
    }

    /**
     * Returns the boolean identified by given key. 
     * If not found an IllegalArgumentException thrown.
     * @param key
     * @param must
     * @return
     * @throws IllegalArgumentException
     */
    public static Boolean getBoolean(String key, boolean must) {
    	return get(key, Boolean.class, must);
    }
}
