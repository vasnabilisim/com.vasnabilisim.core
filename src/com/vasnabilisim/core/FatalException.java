package com.vasnabilisim.core;

import com.vasnabilisim.util.MessageType;

/**
 * FatalException
 * @author Menderes Fatih GUVEN
 */
public class FatalException extends BaseException {
	private static final long serialVersionUID = -3960723003460482878L;

	public FatalException() {
		super(MessageType.Fatal);
	}

	/**
	 * @param message
	 */
	public FatalException(String message) {
		this(message, null);
	}

	/**
	 * @param cause
	 */
	public FatalException(Throwable cause) {
		this(null, cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public FatalException(String message, Throwable cause) {
		super(MessageType.Fatal, message, cause);
	}
}
