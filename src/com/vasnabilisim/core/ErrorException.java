package com.vasnabilisim.core;

import com.vasnabilisim.util.MessageType;

/**
 * ErrorException
 * @author Menderes Fatih GUVEN
 */
public class ErrorException extends BaseException {
	private static final long serialVersionUID = -6037923169169515469L;

	public ErrorException() {
		super(MessageType.Error);
	}

	/**
	 * @param message
	 */
	public ErrorException(String message) {
		super(MessageType.Error, message);
	}

	/**
	 * @param cause
	 */
	public ErrorException(Throwable cause) {
		super(MessageType.Error, cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ErrorException(String message, Throwable cause) {
		super(MessageType.Error, message, cause);
	}

}
